/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import User from '../api/user/user.model';
import config from './environment/';

export default function seedDatabaseIfNeeded() {
  if(config.seedDB) {
    User.find({}).remove()
      .then(() => {
        User.create({
          provider: 'local',
          name: 'Convidado',
          email: 'convidado@example.com',
          password: 'S0lan3a#123'
        }, {
          provider: 'local',
          role: 'admin',
          name: 'Bruno Rafael',
          email: 'bruno.rafha@gmail.com',
          password: 'S0lan3a#123'
        }, {
          provider: 'local',
          role: 'admin',
          name: 'Wolhfagon Araújo',
          email: 'wolhfagon.araujo@gmail.com',
          password: 'S0lan3a#123'
        })
        .then(() => console.log('finished populating users'))
        .catch(err => console.log('error populating users', err));
      });
  }
}
