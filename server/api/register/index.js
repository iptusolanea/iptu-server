'use strict';

var auth = require('../../auth/auth.service');

var express = require('express');
var controller = require('./register.controller');

var router = express.Router();

router.get('/search', controller.search);
router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/', controller.upsert);
router.patch('/:id', controller.patch);
router.delete('/', auth.hasRole('admin'), controller.destroy);

module.exports = router;
