QUADRO_IV = {
    estrutura: {
        "Alvenaria": {
            "Casa": 7,
            "Apto": 21,
            "Sal/Conj": 26,
            "Loja/Sobrel": 21,
            "Loja": 4,
            "Galpão": 18,
            "Telheiro": 3,
            "Industria": 21,
            "Especial": 14

        },
        "Madeira": {
            "Casa": 7,
            "Apto": 21,
            "Sal/Conj": 26,
            "Loja/Sobrel": 21,
            "Loja": 4,
            "Galpao": 18,
            "Telheiro": 3,
            "Industria": 21,
            "Especial": 14
        },
        "Concreto": {
            "Casa": 10,
            "Apto": 21,
            "Sal/Conj": 26,
            "Loja/Sobrel": 21,
            "Loja": 6,
            "Galpao": 20,
            "Telheiro": 4,
            "Industria": 30,
            "Especial": 16
        },
        "Metal/Aço": {
            "Casa": 12,
            "Apto": 25,
            "Sal/Conj": 31,
            "Loja/Sobrel": 28,
            "Loja": 9,
            "Galpao": 22,
            "Telheiro": 5,
            "Industria": 35,
            "Especial": 20
        }
    },
    vedacoes: {
        "Inexistente": {
            "Casa": 0,
            "Apto": 0,
            "Sal/Conj": 0,
            "Loja/Sobrel": 0,
            "Loja": 0,
            "Galpao": 0,
            "Telheiro": 0,
            "Industria": 0,
            "Especial": 0
        },
        "Madeira": {
            "Casa": 11,
            "Apto": 15,
            "Sal/Conj": 14,
            "Loja/Sobrel": 12,
            "Loja": 13,
            "Galpao": 9,
            "Telheiro": 10,
            "Industria": 5,
            "Especial": 4
        },
        "Alvenaria": {
            "Casa": 6,
            "Apto": 10,
            "Sal/Conj": 10,
            "Loja/Sobrel": 9,
            "Loja": 12,
            "Galpao": 7,
            "Telheiro": 9,
            "Industria": 4,
            "Especial": 3
        },
        "Mista (Alv./Madeira)": {
            "Casa": 8,
            "Apto": 8,
            "Sal/Conj": 7,
            "Loja/Sobrel": 6,
            "Loja": 6,
            "Galpao": 6,
            "Telheiro": 5,
            "Industria": 2,
            "Especial": 2
        },
        "Concreto": {
            "Casa": 12,
            "Apto": 16,
            "Sal/Conj": 15,
            "Loja/Sobrel": 16,
            "Loja": 13,
            "Galpao": 10,
            "Telheiro": 10,
            "Industria": 7,
            "Especial": 6
        },
        "Metal/Aço": {
            "Casa": 13,
            "Apto": 17,
            "Sal/Conj": 16,
            "Loja/Sobrel": 17,
            "Loja": 14,
            "Galpao": 11,
            "Telheiro": 11,
            "Industria": 8,
            "Especial": 7
        },
        "Taipa":{
            "Casa": 2,
            "Apto": 8,
            "Sal/Conj": 5,
            "Loja/Sobrel": 0,
            "Loja": 0,
            "Galpao": 2,
            "Telheiro": 8,
            "Industria": 2,
            "Especial": 2
        }
    },
    //cobertura
    coberta:{
        "Palha/Zinco": {
            "Casa": 0,
            "Apto": 0,
            "Sal/Conj": 0,
            "Loja/Sobrel": 0,
            "Loja": 0,
            "Galpao": 0,
            "Telheiro": 0,
            "Industria": 0,
            "Especial": 0
        }, 
        "Telha de barro": {
            "Casa": 7,
            "Apto": 1,
            "Sal/Conj": 1,
            "Loja/Sobrel": 1,
            "Loja": 1,
            "Galpao": 5,
            "Telheiro": 6,
            "Industria": 4,
            "Especial": 1
        }, 
        "Telha amianto":{
            "Casa": 5,
            "Apto": 1,
            "Sal/Conj": 1,
            "Loja/Sobrel": 1,
            "Loja": 1,
            "Galpao": 10,
            "Telheiro": 7,
            "Industria": 5,
            "Especial": 1
        }, 
        "Laje/Concreto": {
            "Casa": 9,
            "Apto": 1,
            "Sal/Conj": 1,
            "Loja/Sobrel": 1,
            "Loja": 1,
            "Galpao": 16,
            "Telheiro": 10,
            "Industria": 6,
            "Especial": 1
        }, 
        "Especial": {
            "Casa": 18,
            "Apto": 1,
            "Sal/Conj": 1,
            "Loja/Sobrel": 2,
            "Loja": 1,
            "Galpao": 23,
            "Telheiro": 17,
            "Industria": 13,
            "Especial": 2
        }
    },
    revestimento:{
        externo:{
            "Sem": {
                "Casa": 0,
                "Apto": 0,
                "Sal/Conj": 0,
                "Loja/Sobrel": 0,
                "Loja": 0,
                "Galpao": 0,
                "Telheiro": 0,
                "Industria": 0,
                "Especial": 0
            }, 
            "Emboço/Reboco": {
                "Casa": 1,
                "Apto": 1,
                "Sal/Conj": 1,
                "Loja/Sobrel": 1,
                "Loja": 1,
                "Galpao": 1,
                "Telheiro": 1,
                "Industria": 1,
                "Especial": 1
            }, 
            "Madeira": {
                "Casa": 1,
                "Apto": 1,
                "Sal/Conj": 1,
                "Loja/Sobrel": 1,
                "Loja": 1,
                "Galpao": 1,
                "Telheiro": 1,
                "Industria": 1,
                "Especial": 1
            }, 
            "Cerâmica": {
                "Casa": 1,
                "Apto": 1,
                "Sal/Conj": 1,
                "Loja/Sobrel": 1,
                "Loja": 1,
                "Galpao": 1,
                "Telheiro": 1,
                "Industria": 1,
                "Especial": 1
            }, 
            "Especial": {
                "Casa": 2,
                "Apto": 1,
                "Sal/Conj": 1,
                "Loja/Sobrel": 1,
                "Loja": 1,
                "Galpao": 1,
                "Telheiro": 1,
                "Industria": 1,
                "Especial": 1
            }
        },
        interno: {
            "Sem": {
                "Casa": 0,
                "Apto": 0,
                "Sal/Conj": 0,
                "Loja/Sobrel": 0,
                "Loja": 0,
                "Galpao": 0,
                "Telheiro": 0,
                "Industria": 0,
                "Especial": 0
            }, 
            "Emboço/Reboco": {
                "Casa": 3,
                "Apto": 3,
                "Sal/Conj": 3,
                "Loja/Sobrel": 3,
                "Loja": 3,
                "Galpao": 3,
                "Telheiro": 0,
                "Industria": 3,
                "Especial": 6
            }, 
            "Madeira": {
                "Casa": 11,
                "Apto": 11,
                "Sal/Conj": 11,
                "Loja/Sobrel": 11,
                "Loja": 11,
                "Galpao": 8,
                "Telheiro": 0,
                "Industria": 8,
                "Especial": 9
            }, 
            "Cerâmica": {
                "Casa": 12,
                "Apto": 12,
                "Sal/Conj": 12,
                "Loja/Sobrel": 12,
                "Loja": 12,
                "Galpao": 7,
                "Telheiro": 0,
                "Industria": 7,
                "Especial": 15
            }, 
            "Especial": {
                "Casa": 15,
                "Apto": 15,
                "Sal/Conj": 15,
                "Loja/Sobrel": 15,
                "Loja": 15,
                "Galpao": 10,
                "Telheiro": 0,
                "Industria": 10,
                "Especial": 18
            }
        }
    },
    pintura: {
        interna: {
            "Inexistente": {
                "Casa": 0,
                "Apto": 0,
                "Sal/Conj": 0,
                "Loja/Sobrel": 0,
                "Loja": 0,
                "Galpao": 0,
                "Telheiro": 0,
                "Industria": 0,
                "Especial": 0
            }, 
            "Caiação": {
                "Casa": 1,
                "Apto": 1,
                "Sal/Conj": 1,
                "Loja/Sobrel": 1,
                "Loja": 1,
                "Galpao": 1,
                "Telheiro": 1,
                "Industria": 1,
                "Especial": 1
            }, 
            "Óleo/Látex": {
                "Casa": 3,
                "Apto": 3,
                "Sal/Conj": 3,
                "Loja/Sobrel": 3,
                "Loja": 3,
                "Galpao": 2,
                "Telheiro": 1,
                "Industria": 2,
                "Especial": 2
            }, 
            "Especial": {
                "Casa": 5,
                "Apto": 5,
                "Sal/Conj": 5,
                "Loja/Sobrel": 5,
                "Loja": 5,
                "Galpao": 4,
                "Telheiro": 1,
                "Industria": 4,
                "Especial": 4
            }
        }, 
        externa: {
            "Inexistente": {
                "Casa": 0,
                "Apto": 0,
                "Sal/Conj": 0,
                "Loja/Sobrel": 0,
                "Loja": 0,
                "Galpao": 0,
                "Telheiro": 0,
                "Industria": 0,
                "Especial": 0
            }, 
            "Caiação": {
                "Casa": 1,
                "Apto": 1,
                "Sal/Conj": 1,
                "Loja/Sobrel": 1,
                "Loja": 1,
                "Galpao": 1,
                "Telheiro": 1,
                "Industria": 1,
                "Especial": 1
            }, 
            "Óleo/Látex": {
                "Casa": 2,
                "Apto": 2,
                "Sal/Conj": 2,
                "Loja/Sobrel": 2,
                "Loja": 2,
                "Galpao": 2,
                "Telheiro": 1,
                "Industria": 2,
                "Especial": 2
            }, 
            "Especial": {
                "Casa": 4,
                "Apto": 4,
                "Sal/Conj": 4,
                "Loja/Sobrel": 4,
                "Loja": 4,
                "Galpao": 3,
                "Telheiro": 1,
                "Industria": 3,
                "Especial": 4
            }
        }
    },
    instalacao_eletrica: {
        "Inexistente": {
            "Casa": 0,
            "Apto": 0,
            "Sal/Conj": 0,
            "Loja/Sobrel": 0,
            "Loja": 0,
            "Galpao": 0,
            "Telheiro": 0,
            "Industria": 0,
            "Especial": 0
        }, 
        "Aparente": {
            "Casa": 2,
            "Apto": 3,
            "Sal/Conj": 2,
            "Loja/Sobrel": 3,
            "Loja": 1,
            "Galpao": 1,
            "Telheiro": 2,
            "Industria": 2,
            "Especial": 6
        }, 
        "Semi-embutida": {
            "Casa": 4,
            "Apto": 4,
            "Sal/Conj": 4,
            "Loja/Sobrel": 5,
            "Loja": 2,
            "Galpao": 1,
            "Telheiro": 2,
            "Industria": 2,
            "Especial": 9
        }, 
        "Embutida": {
            "Casa": 4,
            "Apto": 5,
            "Sal/Conj": 5,
            "Loja/Sobrel": 5,
            "Loja": 3,
            "Galpao": 1,
            "Telheiro": 2,
            "Industria": 2,
            "Especial": 10
        }, 
        "Especial": {
            "Casa": 5,
            "Apto": 7,
            "Sal/Conj": 6,
            "Loja/Sobrel": 6,
            "Loja": 4,
            "Galpao": 2,
            "Telheiro": 2,
            "Industria": 3,
            "Especial": 12
        }
    }, 
    instalacao_sanitaria: {
        "Inexistente": {
            "Casa": 0,
            "Apto": 0,
            "Sal/Conj": 0,
            "Loja/Sobrel": 0,
            "Loja": 0,
            "Galpao": 0,
            "Telheiro": 0,
            "Industria": 0,
            "Especial": 0
        }, 
        "Externa": {
            "Casa": 2,
            "Apto": 3,
            "Sal/Conj": 3,
            "Loja/Sobrel": 3,
            "Loja": 1,
            "Galpao": 1,
            "Telheiro": 1,
            "Industria": 2,
            "Especial": 11
        }, 
        "Interna": {
            "Casa": 8,
            "Apto": 5,
            "Sal/Conj": 5,
            "Loja/Sobrel": 7,
            "Loja": 4,
            "Galpao": 2,
            "Telheiro": 3,
            "Industria": 3,
            "Especial": 14
        }, 
        "Mais de uma interna": {
            "Casa": 12,
            "Apto": 13,
            "Sal/Conj": 12,
            "Loja/Sobrel": 12,
            "Loja": 9,
            "Galpao": 4,
            "Telheiro": 4,
            "Industria": 5,
            "Especial": 21
        }
    },
    piso: {
        "Terra batida": {
            "Casa": 0,
            "Apto": 0,
            "Sal/Conj": 0,
            "Loja/Sobrel": 0,
            "Loja": 0,
            "Galpao": 0,
            "Telheiro": 0,
            "Industria": 0,
            "Especial": 3
        }, 
        "Tijolo/Cimento": {
            "Casa": 1,
            "Apto": 2,
            "Sal/Conj": 1,
            "Loja/Sobrel": 1,
            "Loja": 2,
            "Galpao": 4,
            "Telheiro": 1,
            "Industria": 1,
            "Especial": 4
        }, 
        "Taco/Madeira": {
            "Casa": 3,
            "Apto": 3,
            "Sal/Conj": 3,
            "Loja/Sobrel": 3,
            "Loja": 6,
            "Galpao": 6,
            "Telheiro": 1,
            "Industria": 5,
            "Especial": 5
        }, 
        "Cerâmica": {
            "Casa": 4,
            "Apto": 3,
            "Sal/Conj": 4,
            "Loja/Sobrel": 3,
            "Loja": 6,
            "Galpao": 6,
            "Telheiro": 1,
            "Industria": 7,
            "Especial": 7
        }, 
        "Plástico/PVC": {
            "Casa": 5,
            "Apto": 5,
            "Sal/Conj": 4,
            "Loja/Sobrel": 6,
            "Loja": 12,
            "Galpao": 12,
            "Telheiro": 1,
            "Industria": 10,
            "Especial": 12
        }, 
        "Mármore": {
            "Casa": 6,
            "Apto": 6,
            "Sal/Conj": 6,
            "Loja/Sobrel": 6,
            "Loja": 12,
            "Galpao": 12,
            "Telheiro": 1,
            "Industria": 11,
            "Especial": 14
        }, 
        "Especial": {
            "Casa": 7,
            "Apto": 7,
            "Sal/Conj": 7,
            "Loja/Sobrel": 7,
            "Loja": 14,
            "Galpao": 14,
            "Telheiro": 1,
            "Industria": 12,
            "Especial": 16
        }
    },
    forro: {
        "Inexistente": {
            "Casa": 0,
            "Apto": 0,
            "Sal/Conj": 0,
            "Loja/Sobrel": 0,
            "Loja": 0,
            "Galpao": 0,
            "Telheiro": 0,
            "Industria": 0,
            "Especial": 0
        }, 
        "Madeira": {
            "Casa": 5,
            "Apto": 5,
            "Sal/Conj": 5,
            "Loja/Sobrel": 5,
            "Loja": 5,
            "Galpao": 5,
            "Telheiro": 5,
            "Industria": 5,
            "Especial": 5
        }, 
        "Estuque/Gesso": {
            "Casa": 10,
            "Apto": 10,
            "Sal/Conj": 10,
            "Loja/Sobrel": 10,
            "Loja": 10,
            "Galpao": 10,
            "Telheiro": 10,
            "Industria": 10,
            "Especial": 10
        }, 
        "Laje/Concreto": {
            "Casa": 6,
            "Apto": 6,
            "Sal/Conj": 6,
            "Loja/Sobrel": 6,
            "Loja": 6,
            "Galpao": 6,
            "Telheiro": 6,
            "Industria": 6,
            "Especial": 6
        }, 
        "Chapa/Aço": {
            "Casa": 8,
            "Apto": 8,
            "Sal/Conj": 8,
            "Loja/Sobrel": 8,
            "Loja": 8,
            "Galpao": 8,
            "Telheiro": 8,
            "Industria": 8,
            "Especial": 8
        }, 
        "PVC": {
            "Casa": 8,
            "Apto": 8,
            "Sal/Conj": 8,
            "Loja/Sobrel": 8,
            "Loja": 8,
            "Galpao": 8,
            "Telheiro": 8,
            "Indústria": 8,
            "Especial": 8
        },
    },
    getValues: function(register){
        var tipologia = caracteristicas_da_edificacao.tipologia;
        var estrutura = caracteristicas_da_edificacao.estrutura;
        var vedacoes = caracteristicas_da_edificacao.vedacoes;
        var cobertura = caracteristicas_da_edificacao.coberta;
        var revestimento_interno = caracteristicas_da_edificacao.revestimento.interno;     
        var revestimento_externo = caracteristicas_da_edificacao.revestimento.interno;
        var pintura_externa = caracteristicas_da_edificacao.pintura.externa;
        var pintura_interna = caracteristicas_da_edificacao.pintura.interna;
        var piso = caracteristicas_da_edificacao.piso;
        var forro = caracteristicas_da_edificacao.forro;
        var instalacao_eletrica = caracteristicas_da_edificacao.instalacao_eletrica;
        var instalacao_sanitaria = caracteristicas_da_edificacao.instalacao_sanitaria;


    }
}

module.exports = QUADRO_IV;