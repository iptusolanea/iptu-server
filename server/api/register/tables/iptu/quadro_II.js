var RESIDENCIAL = "Residencial";
QUADRO_II = {
    "A":{
        "nao_residencial" : 0.90,
        "residencial": 0.80,
        test: function(services_lenght){
            return services_lenght <= 5
        }
    },
    "B": {
        "nao_residencial" : 1.00,
        "residencial": 0.90,
        test: function(services_lenght){
            return 5 < services_lenght <= 10;
        }
    },
    "C": {
        "nao_residencial" : 1.10,
        "residencial": 1.00,
        test: function(services_lenght){
            return services_lenght > 10
        }
    },
    __getTotalTrueValues: function(services){
        var services_lenght = 0;
        for(s in services){
            if(services[s]){
                services_lenght++;
            }
        }
        return services_lenght;
    },
    getValues: function(register){
        var len = this.__getTotalTrueValues(register.servicos_urbanos);
        for (var k in QUADRO_II){
            if (QUADRO_II[k].test(len)){
                if(register.caracteristicas_do_terreno.pedologia_solo == RESIDENCIAL){
                    return {
                        categoria_do_terreno : QUADRO_II[k].residencial
                    }
                } else {
                    return {
                        categoria_do_terreno : QUADRO_II[k].nao_residencial
                    }
                }
            }
        }
    }
}

module.exports = QUADRO_II;