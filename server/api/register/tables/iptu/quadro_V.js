var RESIDENCIAL = "Residencial";
QUADRO_V = {
    //número do distrito
    "01": {
        //número do setor
        "01": {
            //tipo de imovel
            "residencial": 1.00,
            "nao_residencial": 1.00
        },
        "02" : {
            "residencial": 1.00,
            "nao_residencial": 1.10
        },
        "03" : {
            "residencial": 1.00,
            "nao_residencial": 1.20
        },
        "04" : {
            "residencial": 1.00,
            "nao_residencial": 1.20
        },
        "05" : {
            "residencial": 1.00,
            "nao_residencial": 1.30
        }
    },
    "02": {
        //número do setor
        "01": {
            //tipo de imovel
            "residencial": 0.90,
            "nao_residencial": 1.00
        },
        "02" : {
            //tipo de imovel
            "residencial": 0.90,
            "nao_residencial": 1.00
        },
        "03" : {
            //tipo de imovel
            "residencial": 0.90,
            "nao_residencial": 1.00
        },
        "04" : {
            //tipo de imovel
            "residencial": 0.90,
            "nao_residencial": 1.00
        },
        "05" : {
            //tipo de imovel
            "residencial": 0.90,
            "nao_residencial": 1.00
        }

    },
    "03": {
        //número do setor
        "01": {
            //tipo de imovel
            "residencial": 0.80,
            "nao_residencial": 0.90
        },
        "02" : {
            //tipo de imovel
            "residencial": 0.80,
            "nao_residencial": 0.90
        },
        "03" : {
            //tipo de imovel
            "residencial": 0.80,
            "nao_residencial": 1.00
        },
        "04" : {
            //tipo de imovel
            "residencial": 0.80,
            "nao_residencial": 1.00
        },
        "05" : {
            //tipo de imovel
            "residencial": 0.80,
            "nao_residencial": 1.10
        }

    }, 
    "04": {
        //número do setor
        "01": {
            //tipo de imovel
            "residencial": 0.70,
            "nao_residencial": 0.80
        },
        "02" : {
            //tipo de imovel
            "residencial": 0.70,
            "nao_residencial": 0.80
        },
        "03" : {
            //tipo de imovel
            "residencial": 0.70,
            "nao_residencial": 0.90
        },
        "04" : {
            //tipo de imovel
            "residencial": 0.70,
            "nao_residencial": 0.90
        },
        "05" : {
            //tipo de imovel
            "residencial": 0.70,
            "nao_residencial": 1.00
        },
        "06" : {
            //tipo de imovel
            "residencial": 0.70,
            "nao_residencial": 1.00
        }
    },
    getValues: function(register){
        var distrito = register.inscricao_cadastral.distrito;
        var setor = register.inscricao_cadastral.setor;
        if(register.caracteristicas_do_terreno.pedologia_solo == RESIDENCIAL){
            return {
                "L" : QUADRO_V[distrito][setor].residencial
            }
        } else {
            return {
                "L" : QUADRO_V[distrito][setor].nao_residencial
            }
        }
        

    }
};

module.exports = QUADRO_V;