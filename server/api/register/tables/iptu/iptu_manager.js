/**
 * @author Bruno Rafael Araujo Vasconcelos
 */


import quadro_I from "./quadro_I";
import quadro_II from "./quadro_II";
import quadro_III from "./quadro_III";
import quadro_IV from "./quadro_IV";
import quadro_V from "./quadro_V";

var iptu_manager = {
    getIptuValues: function(register) {
        var area_do_terreno = register.caracteristicas_do_terreno.dimensoes_do_terreno.area_do_terreno;
        var area_da_edificacao = register.caracteristicas_da_edificacao.dimensoes_das_edificacoes.edificacao_principal.area_construida1;
        var q1 = quadro_I.getValues(register);
        var q2 = quadro_II.getValues(register);
        var q3 = quadro_III.getValues(register);
        var q4 = quadro_IV.getValues(register);
        var q5 = quadro_V.getValues(register);

        return {
            AT: area_do_terreno,
            T: q1.topografia,
            P: q1.pedologia,
            Q: q1.quadra,
            CT: q2.categoria_do_terreno
        }

    }
};

module.exports = iptu_manager;