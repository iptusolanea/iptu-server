QUADRO_I = {
    //número do distrito
    "pedologia": {
        "Normal/Firme": 1.00,
        "Arenoso": 0.90,
        "Rochoso": 0.80,
        "Inundavel": 0.70,
        "Alagado": 0.60,
        "Combinado/Outro": 0.80
    },
    "topografia": {
        "Plano (frente)": 1.00,
        "Aclive (frente)": 0.90,
        "Declive (frente)": 0.80,
        "Inclinado (Frente/Fundo)": 0.70,
        "Irregular (Combinado)": 0.85
    },
    "quadra": {
        "Toda quadra": 1.30,
        "Meio da quadra/Duas frentes": 1.20,
        "Esquina/Mais uma frente": 1.10,
        "Meio da quadra/Uma frente": 1.00,
        "Encravado": 0.90,
        "Gleba": 0.80,
        "Fundos": 0.70,
        "Vila": 0.60
    },
    getValues: function(register){
        return {
            pedologia : QUADRO_I[register.caracteristicas_do_terreno.pedologia_solo],
            topografia: QUADRO_I[register.caracteristicas_do_terreno.topografia_perfil],
            quadra: QUADRO_I[register.caracteristicas_do_terreno.situacao]
        };
    }
};

module.exports = QUADRO_I;