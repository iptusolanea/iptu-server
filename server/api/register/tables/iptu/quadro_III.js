var keys = {
    "TERREO": "Térreo",
    "LOJA": "Loja/Sala",
    "LOJA_SUBS": "LOJA EM SOBRELOJA/SUBSOLO",
    "LOJA_TERREA": "LOJA TÉRREA"
}
QUADRO_III = {
    obsolencia: {
        "--|05" : 1.00,
        "05|--10" : 0.90,
        "10|--20" : 0.80,
        "20|--30" : 0.70,
        "30|--40" : 0.60,
        "40|--50" : 0.50,
        "50|--" : 0.40
    },
    conservacao : {
        "Nova/Ótima": 1.00,
        "Boa": 0.90,
        "Regular": 0.80,
        "Má": 0.70,
        "Precária": 0.60
    },
    posicao_da_edificacao: {
        "ALINHADA/ISOLADA": 0.90,
        "ALINHADA/SUPERPOSTA" : 0.95,
        "ALINHADA/CONJUGADA": 0.85,
        "ALINHADA/GEMINADA": 0.80,
        "RECUADA/ISOLADA": 1.00,
        "RECUADA/SUPERPOSTA": 1.00,
        "RECUADA/CONJUGADA": 0.95,
        "RECUADA/GEMINADA": 0.90,
        "FUNDOS/ISOLADA": 0.80,
        "FUNDOS/SUPERPOSTA": 0.90,
        "FUNDOS/CONJUGADA": 0.75,
        "FUNDOS/GEMINADA": 0.70,
        "APARTAMENTO/FRENTE": 1.00,
        "APARTAMENTO/FUNDOS": 0.90,
        "SALA/CONJUNTO": 1.00,
        "LOJA EM SOBRELOJA/SUBSOLO": 1.00,
        "LOJA TÉRREA": 1.00,
        "GALPÃO": 1.00,
        "TELHEIRO": 1.00,
        "INDÚSTRIA": 1.00,
        "ESPECIAL": 1.00,
    },
    getValues: function(register){
        var pos_h = register.caracteristicas_da_edificacao.posicionamento_h_v.h;
        var pos_v = register.caracteristicas_da_edificacao.posicionamento_h_v.v;
        var situacao = register.caracteristicas_da_edificacao.situacao;
        var tipologia = register.caracteristicas_da_edificacao.tipologia;

        var jsonResponse = {};

        if(QUADRO_III.posicao_da_edificacao[tipologia]){
            jsonResponse["PE"] = QUADRO_III.posicao_da_edificacao[tipologia];
        } else {
            if(tipologia == LOJA){
                if(pos_v == TERREO){
                    jsonResponse["PE"] = QUADRO_III.posicao_da_edificacao[keys.LOJA_TERREA]
                } else {
                    jsonResponse["PE"] = QUADRO_III.posicao_da_edificacao[keys.LOJA_SUBS]
                }
            } else {
                var key = pos_h + "/" + situacao;
                jsonResponse["PE"] = QUADRO_III.posicao_da_edificacao[key];
            }
        }

        jsonResponse["I"] = QUADRO_III.obsolencia[register.caracteristicas_da_edificacao.obsolencia];
        jsonResponse["C"] = QUADRO_III.conservacao[register.caracteristicas_da_edificacao.conservacao];
        
        return jsonResponse
    }
}

module.exports = QUADRO_III;