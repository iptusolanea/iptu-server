QUADRO_II = {
    "A": {
        "valor_do_m2_casa": {
            ufm: 1.00,
            valor: 12.77
        },
        "valor_do_m2_apartamento": {
            ufm: 1.30,
            valor: 16.60
        },
        "valor_do_m2_galpao": {
            ufm: 0.95,
            valor: 12.13
        }
    },
    "B": {
        "valor_do_m2_casa": {
            ufm: 1.50,
            valor: 19.16
        },
        "valor_do_m2_apartamento": {
            ufm: 1.95,
            valor: 24.91
        },
        "valor_do_m2_galpao": {
            ufm: 1.43,
            valor: 18.20
        }
    },
    "C": {
        "valor_do_m2_casa": {
            ufm: 2.00,
            valor: 25.54
        },
        "valor_do_m2_apartamento": {
            ufm: 2.60,
            valor: 33.20
        },
        "valor_do_m2_galpao": {
            ufm: 1.90,
            valor: 24.26
        }
    },
    "D": {
        "valor_do_m2_casa": {
            ufm: 2.50,
            valor: 31.92
        },
        "valor_do_m2_apartamento": {
            ufm: 3.25,
            valor: 41.50
        },
        "valor_do_m2_galpao": {
            ufm: 2.38,
            valor: 30.32
        }
    },
    "E": {
        "valor_do_m2_casa": {
            ufm: 3.00,
            valor: 38.31
        },
        "valor_do_m2_apartamento": {
            ufm: 3.90,
            valor: 49.80
        },
        "valor_do_m2_galpao": {
            ufm: 2.85,
            valor: 36.39
        }
    }, 
    "F": {
        "valor_do_m2_casa": {
            ufm: 3.50,
            valor: 44.70
        },
        "valor_do_m2_apartamento": {
            ufm: 4.55,
            valor: 58.11
        },
        "valor_do_m2_galpao": {
            ufm: 3.33,
            valor: 42.47
        }
    },
    "G": {
        "valor_do_m2_casa": {
            ufm: 4.00,
            valor: 51.08
        },
        "valor_do_m2_apartamento": {
            ufm: 5.20,
            valor: 66.40
        },
        "valor_do_m2_galpao": {
            ufm: 3.80,
            valor: 48.53
        }
    },
    "H": {
        "valor_do_m2_casa": {
            ufm: 4.50,
            valor: 57.46
        },
        "valor_do_m2_apartamento": {
            ufm: 5.85,
            valor: 74.70
        },
        "valor_do_m2_galpao": {
            ufm: 4.28,
            valor: 54.59
        }
    },
    "I": {
        "valor_do_m2_casa": {
            ufm: 5.00,
            valor: 63.85
        },
        "valor_do_m2_apartamento": {
            ufm: 6.50,
            valor: 83.01
        },
        "valor_do_m2_galpao": {
            ufm: 4.75,
            valor: 60.66
        }
    },
    "J": {
        "valor_do_m2_casa": {
            ufm: 5.50,
            valor: 70.24
        },
        "valor_do_m2_apartamento": {
            ufm: 7.15,
            valor: 91.31
        },
        "valor_do_m2_galpao": {
            ufm: 5.23,
            valor: 66.73
        }
    },
    getValues : function(register){

    }
};

module.exports = QUADRO_II;