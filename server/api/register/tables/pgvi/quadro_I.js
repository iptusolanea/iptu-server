var RESIDENCIAL = "Residencial";
QUADRO_I = {
    //número do distrito
    "01": {
        //número do setor
        "01": {
            //tipo de imovel
            "residencial": {
                //valor do UFM
                ufm: 1.20,
                valor: 15.32
            },
            "nao_residencial": {
                //valor do UFM
                ufm: 1.35,
                valor: 17.24
            }
        },
        "02" : {
            "residencial": {
                //valor do UFM
                ufm: 1.15,
                valor: 14.68
            },
            "nao_residencial": {
                //valor do UFM
                ufm: 1.30,
                valor: 16.60
            }
        },
        "03" : {
            "residencial": {
                //valor do UFM
                ufm: 1.10,
                valor: 14.05
            },
            "nao_residencial": {
                //valor do UFM
                ufm: 1.25,
                valor: 15.96
            }
        },
        "04" : {
            "residencial": {
                //valor do UFM
                ufm: 1.00,
                valor: 12.77
            },
            "nao_residencial": {
                //valor do UFM
                ufm: 1.20,
                valor: 15.32
            }
        }
    },
    "02": {
        //número do setor
        "01": {
            //tipo de imovel
            "residencial": {
                //valor do UFM
                ufm: 1.30,
                valor: 16.60
            },
            "nao_residencial": {
                //valor do UFM
                ufm: 1.45,
                valor: 18.52
            }
        },
        "02" : {
            //tipo de imovel
            "residencial": {
                //valor do UFM
                ufm: 1.25,
                valor: 15.96
            },
            "nao_residencial": {
                //valor do UFM
                ufm: 1.35,
                valor: 17.24
            }
        },
        "03" : {
            //tipo de imovel
            "residencial": {
                //valor do UFM
                ufm: 1.15,
                valor: 14.68
            },
            "nao_residencial": {
                //valor do UFM
                ufm: 1.25,
                valor: 15.96
            }
        },
        "04" : {
            //tipo de imovel
            "residencial": {
                //valor do UFM
                ufm: 1.10,
                valor: 14.05
            },
            "nao_residencial": {
                //valor do UFM
                ufm: 1.15,
                valor: 14.68
            }
        }

    },
    "03": {
        //número do setor
        "01": {
            //tipo de imovel
            "residencial": {
                //valor do UFM
                ufm: 1.25,
                valor: 15.96
            },
            "nao_residencial": {
                //valor do UFM
                ufm: 1.35,
                valor: 17.24
            }
        },
        "02" : {
            //tipo de imovel
            "residencial": {
                //valor do UFM
                ufm: 1.20,
                valor: 15.32
            },
            "nao_residencial": {
                //valor do UFM
                ufm: 1.30,
                valor: 16.60
            }
        }

    }, 
    "04": {
        //número do setor
        "01": {
            //tipo de imovel
            "residencial": {
                //valor do UFM
                ufm: 1.00,
                valor: 16.60
            },
            "nao_residencial": {
                //valor do UFM
                ufm: 1.40,
                valor: 17.88
            }
        },
        "02" : {
            //tipo de imovel
            "residencial": {
                //valor do UFM
                ufm: 1.25,
                valor: 15.96
            },
            "nao_residencial": {
                //valor do UFM
                ufm: 1.35,
                valor: 17.24
            }
        }
    },
    getValues : function(register){
        var distrito = register.inscricao_cadastral.distrito;
        var setor = register.inscricao_cadastral.setor;
        var valor_do_m2_do_terreno = QUADRO_I[distrito][setor];

        if(register.caracteristicas_do_terreno.pedologia_solo = RESIDENCIAL){
            return {
                UT: valor_do_m2_do_terreno.residencial
            }
        } else {
            return {
                UT: valor_do_m2_do_terreno.nao_residencial
            }
        }
    }
};

module.exports = QUADRO_I;