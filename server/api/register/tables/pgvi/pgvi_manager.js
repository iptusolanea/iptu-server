/**
 * @author Bruno Rafael Araujo Vasconcelos
 */
import quadro_I from "./quadro_I";
import quadro_II from "./quadro_II";
var pgvi_manager = {
    getPgviValues: function(register) {
        AE = register.caracteristicas_da_edificacao.dimensoes_das_edificacoes.area_total_construida;
        
        return {
            quadro_I.getValues(register)
        }
    }
};

module.exports = pgvi_manager;