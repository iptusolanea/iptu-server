'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './register.events';

var RegisterSchema = new mongoose.Schema({
  identificacao_do_registro: String,
  inscricao_cadastral: {
    distrito: String,
    setor: String,
    quadra: String,
    lote: String,
    unidade: String
  },
  dados_gerais: {
    comandos: {
      inclusao: String,
      alteracao: String,
      cancelamento: String
    },
    inscricao_anterior: {
      cadastro_anterior: String 
    }
  },
  localizacao_do_imovel: {
    tipo_de_logradouro: {
      type: String,
      required: true
    },
    nome_do_logradouro: {
      type: String,
      required: true
    },
    codigo_do_logradouro: String,
    numero: {
      type: String,
      required: true
    },
    complemento: String,
    bairro: {
      type: String,
      required: true
    },
    codigo_bairro: String,
    quadra: String,
    lote: String
  },
  informacoes_do_proprietario: {
    nome_proprietario: {
      type: String,
      required: true
    },
    tipo_logradouro: {
      type: String,
      required: true
    },
    nome_do_logradouro: {
      type: String,
      required: true
    },
    numero: {
      type: String,
      required: true
    },
    complemento: String,
    bairro: {
      type: String,
      required: true
    },
    quadra: String,
    lote: String,
    cidade: {
      type: String,
      required: true
    },
    uf: {
      type: String,
      required: true
    },
    cep: String,
    cpf_cnpj: {
      type: String,
      required: true
    },
    email: String
  },
  caracteristicas_do_imovel: {
    ocupacao_do_lote: String,
    patrimonio: String,
    limites: String,
    uso_do_solo: String,
    arvores: String,
    iptu: String,
  },
  caracteristicas_do_terreno:{
    situacao: String,
    topografia_perfil: String,
    pedologia_solo: String,
    passeio_calcada: String,
    dimensoes_do_terreno: {
      testada_principal: String,
      testada2: String,
      testada3: String,
      testada4: String,
      perimetro: String,
      area_terreno: String
    }
  },
  caracteristicas_da_edificacao: {
    tipologia: String,
    posicionamento_h_v: {
      h: String,
      v: String
    },
    situacao: String,
    estrutura: String,
    vedacoes: String,
    coberta: String,
    forro: String,
    piso: String,
    pintura: {
      interna: String,
      externa: String
    },
    revestimento: {
      interno: String,
      externo: String
    },
    instalacao_eletrica: String,
    instalacao_sanitaria: String,
    padrao: String,
    conservacao: String,
    obsolencia: String,
    dimensoes_das_edificacoes: {
      edificacao_principal: {
        principal_frente: String,
        fundos: String,
        lado_comprimento1: String,
        lado_comprimento2: String,
        lado_comprimento3: String,
        perimetro1: String,
        lado_comprimento4: String,
        lado_comprimento5: String,
        area_construida1: String
      }, 
      edicula_dependencia: {
        principal_frente: String,
        fundos: String,
        lado_comprimento1: String,
        lado_comprimento2: String,
        lado_comprimento3: String,
        perimetro2: String,
        lado_comprimento4: String,
        lado_comprimento5: String,
        area_construida2: String
      },
      apartamento: {
        principal_frente: String,
        lado_comprimento1: String,
        perimetro: String,
        pav_andar: String,
        fundos: String,
        lado_comprimento2: String,
        area_construida: String,
      },
      no_aptos: String,
      no_pavimentos: String,
      perimetro_total: String,
      area_total_construida: String
    }
  },
  servicos_urbanos: {
    pavimentacao: Boolean,
    coleta_de_lixo: Boolean,
    galeria_pluvial: Boolean,
    sargetas_guias: Boolean,
    esgoto_sanitario: Boolean,
    agua_encanada: Boolean,
    energia_eletrica: Boolean,
    telefone: Boolean,
    arborizacao: Boolean,
    internet: Boolean,
    psf: Boolean,
    escola: Boolean
  },
  melhoramentos_servicos: String,
  iptu: {
    valor_venal: String,
    desconto: String,
    valor_do_iptu: String
  },
  informacoes_adicionais: {
    arquiteto_engenheiro: {
        type: String,
        required: true
    },
    cadastrador: {
      type: String,
      required: true
    },
    coordenador_de_turma: {
      type: String,
      required: true
    },
    consultor_tecnico: {
      type: String,
      required: true
    },
    observacoes: String
    //data: Date,
  }
  
})

registerEvents(RegisterSchema);
export default mongoose.model('Register', RegisterSchema);
