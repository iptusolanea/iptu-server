'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var registerCtrlStub = {
  index: 'registerCtrl.index',
  show: 'registerCtrl.show',
  search: 'registerCtrl.search',
  create: 'registerCtrl.create',
  upsert: 'registerCtrl.upsert',
  patch: 'registerCtrl.patch',
  destroy: 'registerCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var registerIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './register.controller': registerCtrlStub
});

describe('Register API Router:', function() {
  it('should return an express router instance', function() {
    registerIndex.should.equal(routerStub);
  });

  describe('GET /api/registers', function() {
    it('should route to register.controller.index', function() {
      routerStub.get
        .withArgs('/', 'registerCtrl.index')
        .should.have.been.calledOnce;
    });
  });

  describe('GET /api/registers/:id', function() {
    it('should route to register.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'registerCtrl.show')
        .should.have.been.calledOnce;
    });
  });

  describe('GET /api/registers/search/:code', function() {
    it('should route to register.controller.search', function() {
      routerStub.get
        .withArgs('/:code', 'registerCtrl.search')
        .should.have.been.calledOnce;
    });
  });

  describe('POST /api/registers', function() {
    it('should route to register.controller.create', function() {
      routerStub.post
        .withArgs('/', 'registerCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  describe('PUT /api/registers/:id', function() {
    it('should route to register.controller.upsert', function() {
      routerStub.put
        .withArgs('/:id', 'registerCtrl.upsert')
        .should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/registers/:id', function() {
    it('should route to register.controller.patch', function() {
      routerStub.patch
        .withArgs('/:id', 'registerCtrl.patch')
        .should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/registers/:id', function() {
    it('should route to register.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'registerCtrl.destroy')
        .should.have.been.calledOnce;
    });
  });
});
