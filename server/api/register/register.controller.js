/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/registers              ->  index
 * POST    /api/registers              ->  create
 * GET     /api/registers/:id          ->  show
 * PUT     /api/registers/:id          ->  upsert
 * PATCH   /api/registers/:id          ->  patch
 * DELETE  /api/registers/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Register from './register.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Registers
export function index(req, res) {
  return Register.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Register from the DB
export function show(req, res) {
  return Register.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Search Register(s) in the DB
export function search(req, res) {
  var code = req.query.code;
  if(!code || code == ""){
    Register.find({}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
  } else {
    return Register.aggregate(
    [{
      $project:{
        codeConcat:{
          $concat:[
            "$inscricao_cadastral.quadra", 
            "$inscricao_cadastral.lote", 
            "$inscricao_cadastral.distrito", 
            "$inscricao_cadastral.setor"
          ]
        }
      }
    },
    {
      $match:{codeConcat:code}
    }
    ]).then(
      (response, error) => {
        var arr = []
        for(var i = 0; i < response.length; i++){
          arr.push({_id:response[i]._id})
        }

        if(arr.length == 0){
          var regex = ".*" + code + ".*";
          Register.find({$or:[
            {"identificacao_do_registro" : code},
            {"informacoes_do_proprietario.nome_proprietario" : {$regex : regex, $options : 'i'}}
          ]}).exec()
          .then(respondWithResult(res))
          .catch(handleError(res));
        } else {
          Register.find({$or: arr}).exec()
          .then(respondWithResult(res))
          .catch(handleError(res));
        }
      }
    );
  }
}

// Creates a new Register in the DB
export function create(req, res) {
  return Register.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Register in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  var register = req.body.params.data;

  return Register.findOneAndUpdate({_id: register._id}, register, 
    {new: true, upsert: true, returnNewDocument: true, runValidators: true}).exec()
      .then(respondWithResult(res))
      .catch(handleError(res));
}

// Updates an existing Register in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Register.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Register from the DB
export function destroy(req, res) {
  return Register.findById(req.query.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
