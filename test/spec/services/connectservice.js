'use strict';

describe('Service: ConnectService', function () {

  // load the service's module
  beforeEach(module('prefeituraServerApp'));

  // instantiate service
  var ConnectService;
  beforeEach(inject(function (_ConnectService_) {
    ConnectService = _ConnectService_;
  }));

  it('should do something', function () {
    expect(!!ConnectService).toBe(true);
  });

});
