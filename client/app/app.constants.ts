'use strict';
const angular = require('angular');

export default angular.module('prefeituraServerApp.constants', [])
  .constant('appConfig', require('../../server/config/environment/shared'))
  .name;
