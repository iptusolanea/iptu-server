'use strict';

export function routeConfig($routeProvider, $locationProvider) {
  'ngInject';
  $routeProvider
    .otherwise({
      redirectTo: '/login'
    });


  $locationProvider.html5Mode(true);
}
