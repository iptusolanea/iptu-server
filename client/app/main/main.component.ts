const angular = require('angular');
const ngRoute = require('angular-route');
const moment = require('moment');

import routing from './main.routes';

export class MainController {
  $http;
  connection;
  socket;
  scope;
  isAdmin;

  //Remember : registers is an array!
  registers = [];
  searchRequest;

  opcoes = {
    caracteristicas_do_imovel :{
      ocupacao_do_lote: [
        "Não construído",  
        "Em ruínas",  
        "Em demolição",  
        "Construção paralisada",  
        "Construção em andamento",  
        "Construído/Edificado", 
      ],
      
      patrimonios: [
        "Particular",
        "Financiado",
        "Público municipal",
        "Público estadual",
        "Público federal",
        "Religioso",
        "Filantrópico",
        "Foreiro/Posseiro",
        "Outro",
      ],

      uso_do_solo: [
        "Sem Uso/Vazio",
        "Residencial",
        "Industrial",
        "Comércio/Serviços",
        "Esporte/Diversão",
        "Agropecuário",
        "Clube/Esporte",
        "Sindicato/Associação",
        "Estacionamento",
        "Saúde",
        "Educação/Cultura",
        "Templo",
      ],

      limites: [
        "Sem cerca/Muro",
        "Cercado",
        "Murado",
        "Cercado/Murado",
      ],

      arvores: [
        "Não tem",  
        "Uma",  
        "Até três",  
        "Mais de três", 
      ],

      iptu: [
        "Normal", 
        "Isento imposto", 
        "Isento taxas", 
        "Isento total", 
        "Imune", 
        "Isento TSU", 
      ],
    }, 
    caracteristicas_do_terreno: {
      situacao: [
        "Meio da quadra/Uma frente", 
        "Esquina/Mais uma frente", 
        "Meio da quadra/Duas frentes", 
        "Fundos", 
        "Encravado", 
        "Gleba", 
        "Vila", 
        "Toda quadra", 
      ],

      topografia_perfil: [
        "Plano (frente)", 
        "Aclive (frente)", 
        "Declive (frente)", 
        "Inclinado (Frente/Fundo)", 
        "Irregular (combinado)", 
      ],

      pedologia_solo: [
        "Normal/Firme", 
        "Arenoso", 
        "Rochoso", 
        "Inundável", 
        "Alagado", 
        "Combinado/Outro", 
      ],

      passeio_calcada: [
        "Existente", 
        "Não existente", 
        "Danificado", 
        "Irregular", 
        "Outro", 
      ]
    },
    caracteristicas_da_edificacao: {
      tipologia: [
        "Casa", 
        "Apto térreo", 
        "Apto superior", 
        "Loja/Sala", 
        "Sobrado", 
        "Comércio/Residência", 
        "Garagem", 
        "Templo", 
        "Depósito", 
        "Indústria", 
        "Barracão", 
        "Telheiro", 
        "Galpão", 
        "Precária", 
        "Especial", 
      ],
      posicionamento_h_v: {
        h : [
          "Alinhada", 
          "Recuada", 
          "Fundos", 
          "Apto frente", 
          "Apto fundos", 
        ],
        v: [
          "Térreo", 
          "1º ao 3º", 
          "4º ao 6", 
          "Acima do 6º", 
        ]
      },
      situacao:[
        "Isolada", 
        "Superposta", 
        "Conjugada 1 lado", 
        "Conjugada 2 lados", 
        "Geminada 1 lado", 
        "Geminada 2 lados", 
        "Mista (Conjugada/Geminada)", 
      ],
      estrutura:[
        "Alvenaria", 
        "Madeira", 
        "Concreto", 
        "Metal/Aço", 
      ],
      vedacoes: [
        "Inexistente", 
        "Madeira", 
        "Alvenaria", 
        "Mista (Alv./Madeira)", 
        "Concreto", 
        "Metal/Aço", 
        "Taipa", 
      ],
      coberta: [
        "Palha/Zinco", 
        "Telha de barro", 
        "Telha amianto", 
        "Laje/Concreto", 
        "Especial", 
      ],
      forro: [
        "Inexistente", 
        "Madeira", 
        "Estuque/Gesso", 
        "Laje/Concreto", 
        "Chapa/Aço", 
        "PVC", 
      ],
      piso: [
        "Terra batida", 
        "Tijolo/Cimento", 
        "Taco/Madeira", 
        "Cerâmica", 
        "Plástico/PVC", 
        "Mármore", 
        "Especial", 
      ],
      pintura: {
        interna: [
          "Inexistente", 
          "Caiação", 
          "Óleo/Látex", 
          "Especial", 
        ],
        externa: [
          "Inexistente", 
          "Caiação", 
          "Óleo/Látex", 
          "Especial", 
        ]
      },
      revestimento:{
        externo:[
          "Sem", 
          "Emboço/Reboco", 
          "Madeira", 
          "Cerâmica", 
          "Especial", 

        ],
        interno:[
          "Sem", 
          "Emboço/Reboco", 
          "Madeira", 
          "Cerâmica", 
          "Especial", 
        ]
      },
      instalacao_eletrica: [
        "Inexistente", 
        "Aparente", 
        "Semi-embutida", 
        "Embutida", 
        "Especial", 
      ],
      instalacao_sanitaria: [
        "Inexistente", 
        "Externa", 
        "Interna", 
        "Mais de uma interna", 
      ],
      padrao: [
        "Alto", 
        "Médio/Normal", 
        "Popular", 
        "Baixo", 
      ],
      conservacao: [
        "Nova/Ótima", 
        "Boa", 
        "Regular",
        "Má",
        "Péssima", 
      ],
      obsolencia: [
        "--|05", 
        "05|--10", 
        "10|--20", 
        "20|--30", 
        "30|--40", 
        "40|--50", 
        "50|--", 
      ]
    },
    melhoramentos_servicos:[
        "Até 5", 
        "Entre 6 e 10", 
        "Entre 11 e 15", 
        "Entre 16 e 20", 
        "21 ou mais", 
    ]
  };

  /*@ngInject*/
  constructor($scope, $http, socket, connection, Auth) {
    this.scope = $scope;
    this.$http = $http;
    this.socket = socket;
    this.isAdmin = Auth.isAdminSync;
    this.connection = connection;

    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('registers');
    });
  }

  $onInit() {
    this.connection.getRegisters().then(response => {
      this.registers = response.data;
    });
  }

  getActualDate = function(){
    var date = moment(new Date());
    return moment(moment.utc(date).valueOf()).format('DD/MM/YYYY');
  }

  saveRegister = function(register){
    if(register._id){
      this.connection.updateRegister(register).then(response => {
        for(var i = 0; i < this.registers.length; i++) {
          if(this.registers[i]._id == response.data._id){
            this.registers[i] = response.data;
          }
        }
        this.socket.syncUpdates('registers', this.registers);
      });
    } else {
      this.connection.saveRegister(register).then(response => {  
        this.registers.push(response.data);
        this.socket.syncUpdates('registers', this.registers);
      });
    }
  }

  removeRegister = function(register){
    this.connection.removeRegister(register._id).then(response => {
      this.registers.splice(this.registers.indexOf(register), 1);
      this.socket.syncUpdates('registers', this.registers);
    })
  }

  searchRegister = function(code){
    if(this.searchRequest){
      clearTimeout(this.searchRequest);
    }

    this.searchRequest = setTimeout(()=> {
      this.connection.searchRegisters(code).
        then(response => {
          this.searchRequest = null;
          this.registers = response.data;
          this.socket.syncUpdates('registers', this.registers);
        }).
        catch(exception => {
          clearTimeout(this.searchRequest);
          this.searchRequest = null;
        });
    }, 800);
  }
}

export default angular.module('prefeituraServerApp.main', [ngRoute])
    .config(routing)
    .component('main', {
      template: require('./main.html'),
      controller: MainController
    }).name;