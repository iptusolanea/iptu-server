'use strict';

export default function routes($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/home', {
      template: '<main></main>'
    });
};

