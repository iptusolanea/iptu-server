'use strict';

describe('Service: connection', function() {
  // load the service's module
  beforeEach(module('prefeituraServerApp.connection'));

  // instantiate service
  var connection;
  beforeEach(inject(function(_connection_) {
    connection = _connection_;
  }));

  it('should do someregistr', function() {
    expect(!!connection).toBe(true);
  });
});
