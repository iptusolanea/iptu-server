'use strict';
const angular = require('angular');

/*@ngInject*/
export function connectionService($http) {
  'ngInject';

  return {
    getRegisterById(id) {
      return $http.get('/api/registers/' + id);
    },
    getRegisters() {
      return $http.get('/api/registers');
    },
    saveRegister(register){
      return $http.post('/api/registers/', register);
    },
    updateRegister(register){
      return $http.put('/api/registers/', {params: {data: register}});
    },
    removeRegister(_id){
      return $http.delete('/api/registers', {params: {id: _id}});
    },
    searchRegisters(code){
      if(!code || code == ""){
        return $http.get('/api/registers');
      }
      return $http.get("/api/registers/search", {params: {code: code}});
    }
  }
	// AngularJS will instantiate a singleton by calling "new" on this function
}

export default angular.module('prefeituraServerApp.connection', [])
  .service('connection', connectionService)
  .name;