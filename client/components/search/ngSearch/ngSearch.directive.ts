'use strict';
const angular = require('angular');

export default angular.module('prefeituraServerApp.directives', [])
  .directive('ngSearch', function() {
    return {
      restrict: 'A',
      scope: "=",
      link: function(scope, element, attrs) {
        element.bind("keydown keypress keyup", function() {
          scope.$ctrl.searchRegister(element.val());
        });
      }
    };
  })
  .name;
