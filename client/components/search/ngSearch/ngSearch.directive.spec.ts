'use strict';

describe('Directive: ngSearch', function() {
  // load the directive's module
  beforeEach(module('prefeituraServerApp.directives'));

  var element,
    scope;

  beforeEach(inject(function($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function($compile) {
    element = angular.element('<ng-search></ng-search>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the ngSearch directive');
  }));
});
